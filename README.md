# Universidade Federal de Goiás

## Projeto 2 do Grupo 1

### Membros:


**Rafael Ratacheski de Sousa Raulino** - *rafaelratacheski@gmail.com* - Master

**Jordy Aparecido Faria de Araújo** - *jordyfaria0@gmail.com* - Developer

**Tiago Santana de Castro** - *tiagosantanadecastro@gmail.com* - Developer

**Adílio Vitor de Oliveira Júnior** - *adiliojr31@hotmail.com* - Developer

**Victor Gomide Ferraz** - *victorgomferraz2@gmail.com* - Developer


## Projeto Cliente websocket - Chat IRC

* Interface web que via proxy se comunica com um servidor IRC.

* Bate-papo entre usuarios em diversos canais, com suporte a várias funções do IRC.

# Manual do usuário

### Passo a passo


### Funções suportadas

* JOIN
* ACTION
* NOTICE
* WHOIS
* LIST
* NICK
* PART

### Eventos suportados

* CHANNEL LIST
* ERROR
* JOIN
* PART
* NICK
* NAMES
* MODE
* QUIT
* MOTD

# Manual do desenvolvedor

Para executar o arquivo javascript é necessário baixar o nodejs.(Para isso rode o comando no linux: *'sudo apt-get install nodejs-legacy'*)

Após isso basta rodar o comando *'node \<arquivo.js>'*.

