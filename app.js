var express = require('express');  // módulo express
var app = express();		       // objeto express
//var app = require('express')();  // módulo express
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');     // processa corpo de requests
var cookieParser = require('cookie-parser'); // processa cookies
var irc = require('irc');
var path = require('path');	// módulo usado para lidar com caminhos de arquivos
var socketio_cookieParser = require('socket.io-cookie');
//clientEntity = require('entity_client.js');


server.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

io.use(socketio_cookieParser);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

//console.log('inicio');

var proxies = {}; // mapa de proxys
var proxy_id = 0;
var lista_canais = [];
var lista_usuarios = [];


/**
 * Implementação do cookie pelo socket.io
 * Referencia: https://udgwebdev.com/criando-um-chat-usando-session-do-express-4-no-socket-io-1-0/
 * TODO Testar cookie
 * */
// const KEY = 'nome-do-cookie';
// const SECRET = 'chave-secreta-aqui!';
// cookie = cookieParser(SECRET);
// app.use(cookie);
// app.use(expressSession({
//     secret: SECRET,
//     name: KEY,
//     resave: true,
//     saveUninitialized: true,
//     store: store
// }));
//
// io.use(function(socket, next) {
//     var data = socket.request;
//     cookie(data, {}, function(err) {
//         var sessionID = data.signedCookies[KEY];
//         store.get(sessionID, function(err, session) {
//             if (err || !session) {
//                 return next(new Error('Acesso negado!'));
//             } else {
//                 socket.handshake.session = session;
//                 return next();
//             }
//         });
//     });
// });


/**
 * HTTP Function
 * Send login if there isn't a cookie
 * Send index if there's a cookie
 */
app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
    // o proxy deve ser criado após o estabelecimento da conexao
    // TODO Test this
    var p = proxy(proxy_id,
                  req.cookies.servidor,
                  req.cookies.nick,
                  req.cookies.canal);
    
    proxy_id++;               
    res.cookie('id', proxy_id);
    res.sendFile(path.join(__dirname, '/index.html'));
      
  }else{
      res.sendFile(path.join(__dirname,'/login.html'))
    }
});


app.post('/login', function (req, res) {
   res.cookie('nick', req.body.nick);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});




/**
 * Communication: From Web Client to NodeJS after to IRC server
 * See in Client
 */
io.on('connection', function (socket) {

  console.log('new conection from '+ socket.id);
  socket.send("conectado");

  //var p = proxy(req.cookies.id,
      //          req.cookies.servidor,
        //        req.cookies.nick,
          //      req.cookies.canal,
            //   socket);
  
  socket.on('message', function (msg) {

    console.log('Message Received: ', msg);
    var irc = proxies[proxy.id].irc_client;
    if(msg[0] == '/'){
      msg = msg.split();
      if(msg[1] == 'join') irc.join(msg[2],msg[3]); // (channel, callback)
      else if(msg[1] == 'part') irc.part(msg[2], msg[3],msg[3]); // (channel, [message, ]callback)
    }else{
      irc.say(irc.opt.channels[0], msg );
    }
  });
});


/**
 * Communication: From IRC server to NodeJS to Web Client
 * Ref: API IRC-Client http://node-irc.readthedocs.io/en/latest/API.html
 * See in Events
 */
/* -----------------------------Proxy-----------------------------------*/
function proxy(id, servidor, nick, canal, socket) {

  var ws = socket;

    // IRC Client
    var irc_client = new irc.Client(servidor,nick,{channels: [canal],});

    // Send a message to client web by websocket.
	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	    ws.send(message);
    });

    // Emitted when the server has finished returning a channel list.
	irc_client.addListener('channellist', function(channel_list) {
        ws.send(channel_list);
        lista_canais = channel_list;
	});

	// Emitted when ever the server responds with an error-type message.
    irc_client.addListener('error', function(message) {
        console.log('error: ', message);
        ws.send('Error: '+error);
    });

    // Emitted when a user joins a channel (including when the client itself joins a channel).
	irc_client.addListener('join', 	function (channel, nick, message) {
	    ws.send(nick+" enter in "+channel+" and "+message);
	});

    // Emitted when a user parts a channel (including when the client itself parts a channel).
	irc_client.addListener('part',function (channel, nick, reason, message) {
        ws.send(nick+" part of "+channel+" because "+reason+" and "+message);
    });

    // Emitted when a user change the nick
    irc_client.addListener('nick', function(oldnick, newnick, channels, message) {
        message = 'NICK: ' + oldnick + ' change to ' + newnick;
        console.log(message);
        ws.send(message);
	});

    // Emitted all nicks of a channel
	irc_client.addListener('names', function(channel,nicks) {
        // for (var i = 0; i < Object.keys(nicks).length; i++) {
        //     lista_usuarios.push(Object.keys(nicks)[i]);
        // }
        ws.send("Channel "+channel+" nicks "+nicks);
    });

    // Emitted when a user change mode
	irc_client.addListener('mode', function(message) {
	    console.log('mode: ', message);
	    ws.send(message);
	});

    // Emitted when a user quit
	irc_client.addListener('quit',function (nick, reason, channels) {
        message = nick+" left because "+reason+" of "+channels;
        ws.send(message);
        console.log(message);
    });

	// Emitted the message of the day when that changed
	irc_client.addListener('motd', function(motd,channel) {
	    console.log('motd: ', motd);
	    ws.send(motd);
	});

    proxies[id] = {"ws":ws,"irc_client":irc_client};
    return proxies[id];
}
