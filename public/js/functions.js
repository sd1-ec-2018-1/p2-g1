/*
  Adiciona <mensagem> no <elemento_id>. 
*/
function adiciona_mensagem(mensagem,elemento_id,timestamp) {
	var novo_elemento = document.createElement('div');
	novo_elemento.id = "mensagem"+timestamp;
	document.getElementById(elemento_id).appendChild(novo_elemento);
	document.getElementById('mensagem'+timestamp).innerHTML=mensagem;
}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}

function iniciar(elemento_id) {
	$("#status").text("Conectado - irc://"+
			Cookies.get("nick")+"@"+
			Cookies.get("servidor")+"/"+
			Cookies.get("canal"));
	carrega_mensagens(elemento_id,0);
}

/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp="0";
function carrega_mensagens(elemento_id, timestamp) {
	var mensagem = "";
	var horario = "";
	$.get("obter_mensagem/"+timestamp, function(data,status) {
		if ( status == "success" ) {
		    var linhas = data;
		    for ( var i = linhas.length-1; i >= 0; i-- ) {
		    	horario = timestamp_to_date(linhas[i].timestamp);
			mensagem = 
				"["+horario+" - "+
				linhas[i].nick+"]: "+
		                linhas[i].msg;
			novo_timestamp = linhas[i].timestamp;
		    	adiciona_mensagem(mensagem,elemento_id,novo_timestamp);
			}
		}
		else {
		    alert("erro: "+status);
		}
		}
	);
	t = setTimeout( 
		function() { 
			carrega_mensagens(elemento_id,novo_timestamp) 
		}, 
		1000);		
}

/*
   Submete a mensagem dos valores contidos s elementos identificados 
   como <elem_id_nome> e <elem_id_mensagem>
*/
function submete_mensagem(elem_id_mensagem) {
	var mensagem= document.getElementById(elem_id_mensagem).value;
	var msg = '{"timestamp":'+Date.now()+','+
		  		'"nick":"'+Cookies.get("nick")+'",'+
				'"msg":"'+mensagem+'"}';
	$.ajax({
		type: "post",
		url: "/gravar_mensagem",
		data: msg,
		success: 
		function(data,status) {
			if (status == "success") {
			    // nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
	});
}

function trocarMode(elemento){
	var usuario = Cookies.get("nick");
	var args = $("#"+elemento).val();
	var comando = "mode/"+usuario+"/"+args;
	$.get(comando, function(data,status) {
		if ( status == "success" ) {
			alert(comando);
		}
	});
}



function novo_canal(elemento){
    var nomeCanal= document.getElementById(elemento).value;
    $.ajax({
        type: "post",
        url: "/adicionarCanal",
        data: nomeCanal,
        sucess:
            function (data,status) {
                if (status == "success") {
                    // todo
                }else {
                    alert("erro:"+status);
                }
            },
        contentType: "application/json",
        dataType: "json"
    });
}


//Função responsável por carregar a lista
function carrega_lista(){
		$.get("obter_lista",function(data,status){
			if(!data){
				carrega_lista();
			}else {
				var lista = data;
				console.log(lista[1].name);
				for (var i=0; i<lista.length;i++){
					var node = document.createElement("LI");
					var textnode = document.createTextNode(JSON.stringify(lista[i]));
					node.appendChild(textnode);
					document.getElementById("lista1").appendChild(node);
				}
			}

		});
}
/*
* Função responsavel pela lista de usuarios ativos
*/
function carrega_usuario(){
		$.get("obter_list_usuario", function(data,status){
				var lista;
				if (data != "undefined"){
					document.getElementById("estado").innerHTML = "Usuarios conectados";
					for(var i =0; i<data.length; i++){
					//	lista += data[i]+"";
					var node = document.createElement("LI");
					var textnode = document.createTextNode(data[i]);
					node.appendChild(textnode);
					document.getElementById("lista").appendChild(node);
				}

			}else{
						document.getElementById("estado").innerHTML = "Carregando lista de usuarios";
				}
			});
				$('ul').empty();
		 t = setTimeout(function(){
		 carrega_usuario();
	 },1000);
}

