// Tratamento dos eventos relacionados a canais do cliente IRC

module.exports = function(proxy) {
  var client = proxy.irc_client;

  client.addListener('message', function(nick, to, text, message) {
    proxy.socket.emit('message', {
      from: nick,
      to: to,
      text: text
    });
  });

  client.addListener('join', function(channel, nick, message) {
    if (nick == proxy.nick)
      proxy.canais.push(channel);

    proxy.socket.emit('join', {
      channel: channel,
      nick: nick
    });
  });

  client.addListener('part', function(channel, nick, reason, message) {
    if (nick == proxy.nick)
      proxy.canais.splice(proxy.canais.indexOf(channel), 1);

    proxy.socket.emit('part', {
      channel: channel,
      nick: nick,
      reason: reason
    });
  });

  client.addListener('names', function(channel, nicks) {
    proxy.socket.emit('names', {
      channel: channel,
      nicks: Object.keys(nicks),
      modifiers: nicks
    });
  });

  client.addListener('quit', function(nick, reason, channels, message) {
    proxy.socket.emit('quit', {
      nick: nick,
      reason: reason,
      channels: channels
    });
  });

  client.addListener('invite', function(channel, from, message) {
    proxy.socket.emit('invite', {
      channel: channel,
      from: from
    });
  });

  client.addListener('kick', function(channel, nick, by, reason, message) {
    proxy.socket.emit('kick', {
      channel: channel,
      nick: nick,
      reason: reason,
      by: by,
    });
  });

  client.addListener('topic', function (channel, topic, nick, message) {
    proxy.socket.emit('topic', {
      channel: channel,
      topic: topic,
      nick: nick
    });
  });

  client.addListener('action', function (from, to, text, message) {
    proxy.socket.emit('action', {
      from: from,
      to: to,
      text: text
    });
  });
}
